import 'package:flutter/material.dart';
import '../pages/beranda.dart';
import '../pages/profil.dart';
import '../pages/program.dart';
import '../pages/kegiatan.dart';
import '../pages/tim.dart';
import '../pages/magz.dart';
import '../pages/dukungan.dart';



class Sidebar extends StatelessWidget {
  const Sidebar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>
        [
          const DrawerHeader(
            decoration: BoxDecoration(
              color: Colors.orangeAccent,
            ),
            child: Text(
              "RETROSQUAD RADIO",
              style: TextStyle(color: Colors.white),
            ),
          ),
          ListTile(
            title: const Text('Beranda'),
            onTap: () =>
            {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const Beranda(title: 'Beranda',))),
            },
          ),
          ListTile(
            title: const Text('Profil'),
            onTap: () =>
            {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const Profil(title: 'Profil',))),
            },
          ),
          ListTile(
            title: const Text('Program'),
            onTap: () =>
            {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const Program(title: 'Program',))),
            },
          ),
          ListTile(
            title: const Text('Kegiatan'),
            onTap: () =>
            {
            Navigator.push(context, MaterialPageRoute(builder: (context) => const Kegiatan(title: 'Kegiatan',))),
            },
          ),
          ListTile(
            title: const Text('Tim'),
            onTap: () =>
            {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const Tim(title: 'Tim',))),
            },
          ),
          ListTile(
            title: const Text('Retro Magz'),
            onTap: () =>
            {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const RetroMagz(title: 'Kegiatan',))),
            },
          ),
          ListTile(
            title: const Text('Dukungan'),
            onTap: () =>
            {
              Navigator.push(context, MaterialPageRoute(builder: (context) => const Dukungan(title: 'Kegiatan',))),
            },
          ),
        ],
      ),
    );
  }
}
