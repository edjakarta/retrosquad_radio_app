import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

Future<ProfileModel> fetchProfile() async {
  final response = await http
      .get(Uri.parse('http://retrosquad.test/api/profile/1'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return ProfileModel.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

class ProfileModel
{
  late int id;
  late String title;
  late String excerpt;
  late String body;

  ProfileModel(this.id, this.title, this.body);

  ProfileModel.fromJson(Map<String, dynamic> parsedJson){
    id = parsedJson['id'];
    title = parsedJson['title'];
    excerpt = parsedJson['excerpt'];
    body = parsedJson['body'];
  }

}