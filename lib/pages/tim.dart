import 'package:flutter/material.dart';
import '../components/sidebar.dart';

class Tim extends StatelessWidget {
  const Tim({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Sidebar(),
      appBar: AppBar(
        title: Text(title),
        backgroundColor: Colors.orangeAccent,
      ),
      body: const Center(
        child: Text("This is Tim page"),
      ),
    );
  }
}