
import 'package:flutter/material.dart';
import 'beranda.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const appTitle = 'RETROSQUAD RADIO';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: appTitle,
      home: Beranda(title: appTitle),
    );
  }
}
