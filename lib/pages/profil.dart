import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import '../components/sidebar.dart';

class Profil extends StatefulWidget {
  const Profil({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<Profil> createState() => _ProfilState();
}

class _ProfilState extends State<Profil> {

  void

  @override
  Widget build(BuildContext context) {
    widget.title;
    return Scaffold(
      drawer: const Sidebar(),
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.orangeAccent,
      ),
      body: const Center(
        child: Text("This is Profil page"),
      ),
    );
  }
}